import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    prefixes = ["Mr.", "Mrs.", "Miss."]
    prefix_ages = {prefix: [] for prefix in prefixes}
    prefix_counts = {prefix: 0 for prefix in prefixes}
    for index, row in df.iterrows():
        name = row['Name']
        age = row['Age']
        prefix = None

        for t in prefixes:
            if t in name:
                prefix = t
                break
        if prefix and not pd.isnull(age):
            prefix_ages[prefix].append(age)
        elif prefix:
            prefix_counts[prefix] += 1

    median_values = [(prefix, prefix_counts[prefix], round(pd.Series(prefix_ages[prefix]).median())) for prefix in prefixes]

    return median_values
